import RenderProxy, { SizeStruct } from './base';
import { WebGLRenderer, Scene, Camera, Vector2 } from 'three';

export default class FullResRenderProxy extends RenderProxy {
  private renderer: WebGLRenderer;

  constructor() {
    super();

    this.renderer = new WebGLRenderer();
    this.renderer.autoClear = false;
  }

  public onResize(width: number, height: number): void {
    this.renderer.setSize(width, height);

    const { style } = this.renderer.domElement;
    style.width = '100%';
    style.height = '100%';
  }

  public getSize(): SizeStruct {
    const size = new Vector2();
    const { x: width, y: height } = this.renderer.getSize(size);
    return { width, height };
  }

  public startRender(): void {
    this.renderer.clear();
  }

  public render(scene: Scene, camera: Camera): void {
    this.renderer.render(scene, camera);
  }

  public setContainer(container: HTMLElement): void {
    container.childNodes.forEach(container.removeChild.bind(container));
    container.appendChild(this.renderer.domElement);
  }
}
