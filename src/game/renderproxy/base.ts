import { Camera, Scene } from 'three';

export type SizeStruct = {
  width: number;
  height: number;
};

export default class RenderProxy {
  public getSize(): SizeStruct {
    throw new Error('Not implemented');
  }

  public onResize(_width: number, _height: number): void {
    throw new Error('Not implemented');
  }

  public startRender(): void {
    // Nothing to do
  }

  public render(_scene: Scene, _camera: Camera): void {
    throw new Error('Not implemented');
  }

  public finishRender(): void {
    // Nothing to do
  }

  public setContainer(_container: HTMLElement): void {
    throw new Error('Not implemented');
  }
}
