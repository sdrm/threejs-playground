import RenderProxy, { SizeStruct } from './base';
import {
  WebGLRenderTarget,
  Mesh,
  Scene,
  OrthographicCamera,
  NearestFilter,
  PlaneGeometry,
  MeshBasicMaterial,
  WebGLRenderer,
  Camera,
} from 'three';

const BASE_SIZE = 270;

export default class LowResRenderProxy extends RenderProxy {
  private renderer: WebGLRenderer;
  private renderTarget: WebGLRenderTarget;
  private renderPlane!: Mesh;
  private screenScene: Scene;
  private screenCamera: OrthographicCamera;

  private screenWidth!: number;
  private screenHeight!: number;

  constructor() {
    super();

    this.renderer = new WebGLRenderer();
    this.renderer.autoClear = false;

    this.screenScene = new Scene();
    this.screenCamera = new OrthographicCamera(-1, 1, 1, -1, -1, 1);
    this.renderTarget = new WebGLRenderTarget(480, 270);
    this.renderTarget.texture.generateMipmaps = false;
    this.renderTarget.texture.minFilter = NearestFilter;
    this.renderTarget.texture.magFilter = NearestFilter;

    const planeGeometry = new PlaneGeometry(2, 2);
    const planeMaterial = new MeshBasicMaterial({ map: this.renderTarget.texture });
    this.renderPlane = new Mesh(planeGeometry, planeMaterial);
    this.screenScene.add(this.renderPlane);
  }

  public onResize(width: number, height: number): void {
    if (width > height) {
      this.screenWidth = BASE_SIZE * (width / height);
      this.screenHeight = BASE_SIZE;
    } else {
      this.screenWidth = BASE_SIZE;
      this.screenHeight = BASE_SIZE * (height / width);
    }

    this.renderTarget.setSize(Math.floor(this.screenWidth), Math.floor(this.screenHeight));

    const scaleX = Math.max(1, Math.round(width / this.screenWidth));
    const scaleY = Math.max(1, Math.round(height / this.screenHeight));

    this.renderer.setSize(this.screenWidth * scaleX, this.screenHeight * scaleY);

    const { style } = this.renderer.domElement;
    style.width = '100%';
    style.height = '100%';
  }

  public getSize(): SizeStruct {
    return {
      width: this.screenWidth,
      height: this.screenHeight,
    };
  }

  public startRender(): void {
    this.renderer.setRenderTarget(this.renderTarget);
    this.renderer.clear();
  }

  public render(scene: Scene, camera: Camera): void {
    this.renderer.render(scene, camera);
  }

  public finishRender(): void {
    this.renderer.setRenderTarget(null);
    this.renderer.render(this.screenScene, this.screenCamera);
  }

  public setContainer(container: HTMLElement): void {
    container.childNodes.forEach(container.removeChild.bind(container));
    container.appendChild(this.renderer.domElement);
  }
}
