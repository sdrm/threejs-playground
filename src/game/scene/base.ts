import GameApp from 'game/app';
import RenderProxy from 'game/renderproxy/base';

// eslint-disable-next-line @typescript-eslint/no-explicit-any
export type SceneUserdata = Record<string, any>;

export default class SceneBase {
  protected app: GameApp;

  protected constructor(app: GameApp) {
    console.debug(`${this.constructor.name}: constructor`);
    this.app = app;
  }

  public onEnter(_userdata?: SceneUserdata): void {
    console.debug(`${this.constructor.name}: onEnter()`);
  }

  public onLeave(): void {
    console.debug(`${this.constructor.name}: onLeave()`);
  }

  public onDestroy(): void {
    console.debug(`${this.constructor.name}: onDestroy()`);
  }

  public onResize(_width: number, _height: number): void {
    console.debug(`${this.constructor.name}: onResize()`);
  }

  public onUpdate(_frameTime: number, _renderer: RenderProxy): true | void {
    // Nothing to do
  }

  public onProcessInput(/* _input: Input, */ _frameTime: number): void {
    // Nothing to do
  }
}
