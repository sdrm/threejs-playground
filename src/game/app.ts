import RenderProxy from './renderproxy/base';
import SceneBase, { SceneUserdata } from './scene/base';
import WorldScene from './world/scene';
import FullResRenderProxy from './renderproxy/fullres';
// import LowResRenderProxy from './renderproxy/lowres';

export default class GameApp {
  private renderProxy: RenderProxy;

  private container: HTMLElement;

  // Main loop variables
  private lastTimestamp = 0;

  // Scene management variables
  private sceneStack: SceneBase[] = [];
  private currentScene?: SceneBase;
  private isDirty = false;
  private popUserdata?: SceneUserdata; // Data to pass to the scene when returning from a child scene

  public constructor(container: HTMLElement) {
    this.update = this.update.bind(this);

    this.container = container;

    this.renderProxy = new FullResRenderProxy();
    // this.renderProxy = new LowResRenderProxy();

    this.switchTo(new WorldScene(this));
  }

  public run(): void {
    this.onResize();
    window.addEventListener('resize', this.onResize.bind(this));

    this.renderProxy.setContainer(this.container);

    this.update(this.lastTimestamp);
  }

  public onResize(): void {
    const { clientWidth: width, clientHeight: height } = this.container;
    this.renderProxy.onResize(width, height);

    const { width: screenWidth, height: screenHeight } = this.renderProxy.getSize();
    this.sceneStack.forEach((scene) => {
      scene.onResize(screenWidth, screenHeight);
    });
  }

  public update(timestamp: number): void {
    requestAnimationFrame(this.update);

    // Calculate frametime
    const frameTime = (timestamp - this.lastTimestamp) / 1000;
    this.lastTimestamp = timestamp;

    // Trigger onLeave and onEnter events when there's a new scene
    if (this.sceneStack[0] !== this.currentScene) {
      if (this.currentScene) this.currentScene.onLeave();

      this.currentScene = this.sceneStack[0];
      this.currentScene.onEnter(this.popUserdata);
      this.popUserdata = undefined;
    }

    // Reset the dirty flag
    this.isDirty = false;

    // Process input from only the current scene
    // Don't render if scene stack stack state gets dirty
    this.currentScene.onProcessInput(frameTime);
    if (this.isDirty) return;

    // Update all scenes in the scene stack
    // Abort as soon as scene stack state is dirty
    // Abort as soon as a scene returns `true`
    this.renderProxy.startRender();
    this.sceneStack.some((scene) => {
      return scene.onUpdate(frameTime, this.renderProxy) !== true || this.isDirty;
    });
    this.renderProxy.finishRender();
  }

  public clear(): GameApp {
    // Call onLeave() on the current scene
    if (this.currentScene) {
      this.currentScene.onLeave();
      this.currentScene = undefined;
    }

    // Call onDestroy() on all scenes of the current stack
    this.sceneStack.forEach((scene) => scene.onDestroy());

    // Reset the current stack
    this.sceneStack = [];
    this.isDirty = true;

    return this;
  }

  public push(newScene: SceneBase): GameApp {
    // Add the scene to the scene stack
    this.sceneStack.unshift(newScene);
    this.isDirty = true;

    // initial resize of the scene
    const { width, height } = this.renderProxy.getSize();
    newScene.onResize(width, height);

    return this;
  }

  public switchTo(newScene: SceneBase): GameApp {
    // Clear all the scenes, then push the new one
    return this.clear().push(newScene);
  }

  public pop(userdata: SceneUserdata): GameApp {
    // Don't do anything if there's no scene
    if (this.sceneStack.length === 0) return this;

    // Store pop userdata to be passed on the next scene switch
    this.popUserdata = userdata;

    // Call onLeave() if the current scene is the last scene
    if (this.currentScene == this.sceneStack[0]) {
      this.currentScene.onLeave();
      this.currentScene = undefined;
    }

    // Destroy the last scene
    this.sceneStack[0].onDestroy();
    this.sceneStack.unshift();

    return this;
  }
}
