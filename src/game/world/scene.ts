import { BufferGeometry, BufferAttribute, Mesh, TextureLoader, NearestFilter, Scene, PerspectiveCamera } from 'three';
import Material from 'game/graphics/material';

import grassTextureURL from 'assets/tileset/grass.png';
import RenderProxy from 'game/renderproxy/base';
import SceneBase from 'game/scene/base';
import GameApp from 'game/app';

export default class WorldScene extends SceneBase {
  private chunkMesh!: Mesh;

  private scene: Scene;
  private camera: PerspectiveCamera;

  constructor(app: GameApp) {
    super(app);

    this.scene = new Scene();

    this.camera = new PerspectiveCamera(75, 480 / 270, 0.1, 1000);
    this.camera.position.z = 2;
    this.camera.position.y = -20;
    this.camera.lookAt(0, 0, -10);

    this.createChunk();
  }

  public onUpdate(_frameTime: number, renderer: RenderProxy): void {
    renderer.render(this.scene, this.camera);
  }

  public onResize(width: number, height: number): void {
    this.camera.aspect = width / height;
    this.camera.updateProjectionMatrix();
  }

  private createChunk(): void {
    const CHUNK_SIZE = 64;

    const vertices = new Float32Array(18 * CHUNK_SIZE * CHUNK_SIZE);
    const uvs = new Float32Array(12 * CHUNK_SIZE * CHUNK_SIZE);
    for (let y = 0; y < CHUNK_SIZE; ++y) {
      for (let x = 0; x < CHUNK_SIZE; ++x) {
        const i = (x + y * CHUNK_SIZE) * 18;
        vertices[i + 0] = 0.0 + x;
        vertices[i + 1] = 0.0 + y;
        vertices[i + 2] = x == 22 ? 2.0 : 0.0;

        vertices[i + 3] = 1.0 + x;
        vertices[i + 4] = 0.0 + y;
        vertices[i + 5] = x == 22 ? 2.0 : 0.0;

        vertices[i + 6] = 1.0 + x;
        vertices[i + 7] = 1.0 + y;
        vertices[i + 8] = x == 22 ? 2.0 : 0.0;

        vertices[i + 9] = 1.0 + x;
        vertices[i + 10] = 1.0 + y;
        vertices[i + 11] = x == 22 ? 2.0 : 0.0;

        vertices[i + 12] = 0.0 + x;
        vertices[i + 13] = 1.0 + y;
        vertices[i + 14] = x == 22 ? 2.0 : 0.0;

        vertices[i + 15] = 0.0 + x;
        vertices[i + 16] = 0.0 + y;
        vertices[i + 17] = x == 22 ? 2.0 : 0.0;

        const j = (x + y * CHUNK_SIZE) * 12;
        uvs[j + 0] = 0.0;
        uvs[j + 1] = 0.0;

        uvs[j + 2] = 1.0;
        uvs[j + 3] = 0.0;

        uvs[j + 4] = 1.0;
        uvs[j + 5] = 1.0;

        uvs[j + 6] = 1.0;
        uvs[j + 7] = 1.0;

        uvs[j + 8] = 0.0;
        uvs[j + 9] = 1.0;

        uvs[j + 10] = 0.0;
        uvs[j + 11] = 0.0;
      }
    }

    const geometry = new BufferGeometry();
    geometry.setAttribute('position', new BufferAttribute(vertices, 3));
    geometry.setAttribute('uv', new BufferAttribute(uvs, 2));

    const material = new Material({});

    this.chunkMesh = new Mesh(geometry, material);
    this.scene.add(this.chunkMesh);

    this.chunkMesh.position.x = -CHUNK_SIZE / 2;
    this.chunkMesh.position.y = -CHUNK_SIZE / 2;

    const loader = new TextureLoader();
    loader.load(
      grassTextureURL,
      texture => {
        texture.minFilter = NearestFilter;
        texture.magFilter = NearestFilter;

        material.uniforms.map.value = texture;
      },
      undefined,
      err => {
        console.error('An error occured while loading texture', err);
      },
    );
  }
}
