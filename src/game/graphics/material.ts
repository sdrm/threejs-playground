import { ShaderMaterial, Texture, Vector2, DoubleSide } from 'three';

type MaterialOptions = {
  map?: Texture;
};

export default class Material extends ShaderMaterial {
  public constructor(options: MaterialOptions) {
    super({
      uniforms: {
        map: {
          value: options.map,
        },
        angle: {
          value: 0.015,
        },
        origin: {
          value: new Vector2(0, 0),
        },
      },
      side: DoubleSide,
      vertexShader: `
        uniform float angle;
        uniform vec2 origin;
        varying vec2 vUv;

        void main() {
          vec4 transformed = modelMatrix * vec4(position, 1.0) - vec4(0.0, origin, 0.0);

          float cs = cos(- angle * transformed.y);
          float sn = sin(- angle * transformed.y);
          transformed.y = transformed.y * cs - transformed.z * sn;
          transformed.z = transformed.y * sn + transformed.z * cs;

          transformed += vec4(0.0, origin, 0.0);

          gl_Position = projectionMatrix * viewMatrix * transformed;
          vUv = uv;
        }
      `,
      fragmentShader: `
      uniform sampler2D map;
      varying vec2 vUv;

        void main() {
          vec4 texelColor = texture2D(map, vUv);
          // gl_FragColor = texelColor;
          gl_FragColor = vec4(vUv.x, vUv.y, 1, 1);
        }
      `,
    });

    // window.a = this.uniforms.angle
  }
}
