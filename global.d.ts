declare module 'assets/*' {
  const content: string;
  export default content;
}

// Somewhat not included in the DOM typings
declare class FontFace {
  public constructor(fontFamily: string, fontSource: string);
  public load(): Promise<void>;

  public family: string;
}
